<?php

use \Symfony\Component\Validator\Validation;
use \Symfony\Component\Validator\Constraints as Assert;

trait ControlledProperties {
    function __get($name)
    {
        throw new \InvalidArgumentException("Cannot Read. No such property '$name'");
    }

    function __set($name, $value)
    {
        throw new \InvalidArgumentException("Cannot Write. No such property '$name'");
    }   

    function setPropertiesFromArray($attributes)
    {
        foreach ( $attributes as $k => $v ) {
            $this->{$k} = $v;
        }
        $this->validate(true);
    }

    function getPropertiesAsArray() 
    {
        $properties = get_object_vars($this);
        $array;

        foreach ( $properties as $k => $v ) {
            $method = 'get' . ucfirst($k);
            if ( method_exists($this, $method) ) {
                $array[$k] = $this->$method();
            }
        }

        return $array;
    }

    function isValid()
    {
        $violations = $this->validate(false);
        return $violations->count() ? true : false;
    }

    function validate($throwException = false)
    {
        $validator = Validation::createValidator();
        $rules = new Assert\Collection($this->constraints());
        $violations = $validator->validateValue(get_object_vars($this), $rules);

        if ( $violations->count() && $throwException ) {           
            throw new \InvalidArgumentException("Inavlid values: " . $violations);    
        } 
        return $violations;
    }

    abstract function constraints();
}
